# Local monitoring of felix-star

Local monitoring of felix-star is provided by Prometheus and Grafana.


## Description

The following describes the different applications involved in the local monitoring.


### Data Source

Monitoring/statistics data is written into a fifo by felix-star (felix-tohost, felix-toflx) processes.
It is the same information as is used by felix2atlas which injects it into the ATLAS P1 monitoring system.
For local monitoring this fifo is read by a script felix-stats2prometheus which is supplied in the felix-star project. This script launches a web-server on ```http://localhost:8000``` which should show the latest monitoring information in text form, for example:
```
# HELP felix_tohost_dma_free_MB
# TYPE felix_tohost_dma_free_MB gauge
felix_tohost_dma_free_MB{device="0",dma="0",hostname="macdunspro.dyndns.cern.ch"} 1000.0
felix_tohost_dma_free_MB{device="0",dma="1",hostname="macdunspro.dyndns.cern.ch"} 992.0
felix_tohost_dma_free_MB{device="0",dma="2",hostname="macdunspro.dyndns.cern.ch"} 1018.0
```


### Prometheus

A prometheus server scrapes the data from ```http://localhost:8000``` and collects it into a database. This database is available in prom_data, mounted from the current directory. The data in this database is made available to Grafana as a data source under ```http://localhost:9090/targets```.


### Grafana

The graphs and histograms are divided into tohost/fromhost and two sections on an fid or elink basis.
Grafana selects the datatasource from Prometheus and has a default dashboard available to show FELIX metrics. Local grafana is available on ```http://localhost:3000```


## Running

You can run the local monitoring in a docker image. At any rate you need to run felix-stats2prometheus outside the docker by executing:
```
felix-stats2prometheus <fifo>
```

NOTE: it was tried to also add felix-stats2prometheus to the docker image (directory stats2prometheus) and to have the fifo as the interface between the felix-star processes and the docker. Both mounting the fifo and mounting the directory with the fifo did not result in a fifo that could be written by the host and read by the docker-image (June-2024). Maybe at a later stage this can be revisited. For now the interface between host and the docker image is the felix-stats2prometheus webserver at port 8000.


### Running inside Docker

You need docker and docker-compose installed. To setup the docker env you need to run once:
```
./add_uid_gid_to_env.sh
```
which adds your uid and gid so that the files written from Prometheus are written with your account.

Clean up old resources:
```
docker system prune
```

To run both Prometheus and Grafana execute:
```
docker compose up -d
```
Prometheus is then available under ```http://localhost:9090``` and
Grafana under ```http://localhost:3000```

To see the running containers, execute:
```
docker ps
```

which results in something like:
```
CONTAINER ID   IMAGE                    COMMAND                  CREATED              STATUS              PORTS                    NAMES
f293ba8917ce   grafana/grafana          "/run.sh"                About a minute ago   Up About a minute   0.0.0.0:3000->3000/tcp   grafana-local
6635ec176c9a   prom/prometheus:latest   "/bin/prometheus --c…"   About a minute ago   Up About a minute   0.0.0.0:9090->9090/tcp   prometheus-local
```

To switch off Prometheus and Grafana execute:
```
docker compose down -v
```


### Running as separate processes

Running like this is not suggested at this time, however all configuration files are available but may need some changes.


## Configuration and Implementation

### Data Source

A fifo is used by the felix-star processes to be fully independent of any "home"-made library to publish information. It is written in ndjson (newline delimiter json) format where every line is valid json. A fifo in unix can be read by one process but written by many. The writes are atomic as long as each write is less than the size of a page (currently 4 kbyte). Each line therefore writes only part of dictionary, see example below.
This does mean that the key, consisting of hostname, device, dma, buffer and fid, needs to be (partially) rewritten in every line, which is some overhead.
However this keeps each line to a length of less than 500 bytes, leaving more than enough room to add future metrics to the output.
```
{"ts": "2024-06-28T16:02:11Z", "host": {"MacDunsPro": {"app": {"fromhost": {"device": {"3": {"dma": {"3": {"buffer": {"1": {"fid": {"0x1000000802f78000": {"msgs": 57, "bytes": 9854, "max_msg_size": 9000, "dropped_msgs": 0, "rate_msg_Hz": 11, "rate_msg_Mbps": 38}}}}}}}}}}}}}
```

The script felix-stats2prometheus reads the fifo in ndjson format and books the metrics under the name:

```
felix_<tohost|fromhost>_<metric>
```

and uses the following labels in this order and based on availability (tree-depth):

* hostname
* device
* dma
* buffer
* fid


### Prometheus

The Prometheus server takes a single configuration file, scrapes the felix data source for metrics and publishes its own metrics.
A thing to look into is the timestamps, scraping is done every 5 seconds, while values are published every second. The timestamp of
felix is ignored, while the one of Prometheus is added (internally). There may be a way to get the timestamp from felix to be used or
to at least ignore metrics that have an out of date timestamp (old updates).

An image is built from the latest Prometheus with the configuration supplied in ```prometheus/prometheus.yml```
The ```prom_data``` directory is mounted to keep the history. If not found history is only kept while running the docker image.

### Grafana

The Grafana configuration consists of a FELIX-local Dashboard which has no datasource selected. This will select the default data source.
The default datasource is configured by a configuration file.

An image is built from the latest Grafana with a dashboard config in ```grafana/dashboards/all.yml``` and ```grafana/dashboards/felix-local.json``` dashboard file. The default datasource is set in ```grafana/datasources/datasource.yml```. Some environment variables in ```compose.yml`` setup the grafana running without login prompt or authentication.


### Docker Image

The docker image which is run is composed of both the Prometheus and Grafana images described above. The docker compose command is used to do so.


## Run a simulation on a Mac

### felix-star

- clone felix-star and its sub-modules
- docker run -p 8000:8000 -v `pwd`:/felix-master -it gitlab-registry.cern.ch/atlas-tdaq-felix/felix-build-image:el9
- in the container run:
- cd felix-master/felix-star
- source python_env/bin/activate
- source cmake_tdaq/bin/setup.sh
- cmake_config
- cd x86_64-el9-gcc13-opt
- make -j 8
- mkfifo fifo
- ./felix-stats-generate fifo

### stats2prometheus

- clone felix-monitor and its sub-modules
- docker container ls
- not the CONTAINER ID
- docker exec -it bd6abbe433e6 /bin/bash
- in the container
- cd felix-master/felix-monitor
- source python_env/bin/activate
- source cmake_tdaq/bin/setup.sh
- cmake_config
- cd x86_64-el9-gcc13-opt
- make -j 8
- ./felix-stats2prometheus ../../felix-star/x86_64-el9-gcc13-opt/fifo

- look at http://localhost:8000

### prometheus and grafana

- in felix-monitor (outside container)
- to clean up: docker system prune
- docker compose up -d

- look at http://localhost:9090 status targets
- look at http://localhost:3000 for Grafana
