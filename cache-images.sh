#!/bin/sh

docker pull prom/prometheus
docker tag prom/prometheus gitlab-registry.cern.ch/atlas-tdaq-felix/felix-monitor/prometheus
docker push gitlab-registry.cern.ch/atlas-tdaq-felix/felix-monitor/prometheus

docker pull grafana/grafana
docker tag grafana/grafana gitlab-registry.cern.ch/atlas-tdaq-felix/felix-monitor/grafana
docker push gitlab-registry.cern.ch/atlas-tdaq-felix/felix-monitor/grafana
